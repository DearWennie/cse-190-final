# CSE190 Final Project #

## Q-Learning ##
The implementation of Q-learning algorithm is in cse_190_assi_3

To run simply type:

roslaunch cse_190_assi_3 solution_python.launch

## ROS Related ##
The implementation of a URDF robot model with gazebo and teleop is located in the folder cse_190_final

my_robot_control - contains the yaml file for joint controller and teleop python file

my_robot_description - contains the urdf/xacro files for the robot, wheels and materials as well as a launch file for only rviz (2d only)

my_robot_gazebo - contains the main launch file that launches gazebo and rviz and configure the robot to be ready for teleop control

To run the simulation in gazebo and rviz do not try virtual machine. 

Run the following command:

roslaunch my_robot_gazebo my_robot_gazebo.launch
python keyboad_teleop.py