import random,util,math,bisect
from read_config import read_config

class QLearningAgent():

    def __init__(self, epsilon, alpha, uncertainty):

        self.config = read_config()
        
        self.start = self.config["start"]
        self.goal = self.config["goal"]
        
        self.max_steps = self.config["max_iterations"]
        
        self.rewards = {}
        self.rewards["step"] = self.config["reward_for_each_step"]
        self.rewards["wall"] = self.config["reward_for_hitting_wall"]
        self.rewards["goal"] = self.config["reward_for_reaching_goal"]
        self.rewards["pit"] = self.config["reward_for_falling_in_pit"]
        
        self.width = self.config["map_size"][1]
        self.height = self.config["map_size"][0]

        self.probs = []
        self.probs.append(self.config["prob_move_forward"])
        self.probs.append(self.config["prob_move_backward"])
        self.probs.append(self.config["prob_move_left"])
        self.probs.append(self.config["prob_move_right"])
        
        self.uncertainty = uncertainty
        self.weight = 20
        self.discount = self.config["discount_factor"]
        self.move_list = self.config['move_list']

        self.walls = self.config['walls']
        self.pits = self.config['pits']

        self.epsilon = epsilon
        self.alpha = alpha

        self.qval = util.Counter()

        self.numChoose = util.Counter()


    def getLegalActions(self,state):
        y,x = state
        actions = []

        states = [y,x]
        if states in self.walls or states in self.pits or states == self.goal:
          return actions

        for move in self.move_list:
          delta_y, delta_x = move
          actions.append((delta_y,delta_x))
          # new_y = y + delta_y
          # new_x = x + delta_x
          
          # if new_x >= 0 and new_x < self.width and new_y >= 0 and new_y < self.height:
          #   if [new_y, new_x] not in self.walls and not [new_y, new_x] in self.pits:
          #     actions.append((delta_y,delta_x))

        return actions


    def getQValue(self, state, action):

        if self.numChoose[state,action] == 0:
          return 0.0
        else:
          return self.qval[state,action]

    def computeValueFromQValues(self, state):
        """
          Returns max_action Q(state,action)
          where the max is over legal actions.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return a value of 0.0.
        """

        legal_actions=self.getLegalActions(state)

        if len(legal_actions) == 0:
          return 0.0
          # if state in self.walls:
          #   return self.rewards["wall"]
          # elif state in self.pits:
          #   return self.rewards["pit"]
          # elif state == self.goal:
          #   return self.rewards["goal"]
          # else:
          #   return self.qval[state, None]

        else:
          max_qval = -999999999
          for act in legal_actions:
            qval = self.getQValue(state,act)
            if self.numChoose[state,act] != 0:
              qval += self.weight / float(self.numChoose[state,act])
            if max_qval < qval:
              max_qval = qval

          return max_qval
        

    def computeActionFromQValues(self, state):
        """
          Compute the best action to take in a state.  Note that if there
          are no legal actions, which is the case at the terminal state,
          you should return None.
        """

        legal_actions=self.getLegalActions(state)

        # if there are no legal actions, then return None
        if not legal_actions:
          return None

        else:
          # initialize variables
          max_act = None
          max_qval= None

          for action in legal_actions:

            curr_qval = self.getQValue(state,action)

            if max_qval < curr_qval or max_qval == None:
              max_qval = curr_qval
              max_act = action

          return max_act

    def getAction(self, state):


        legalActions = self.getLegalActions(state)

        action = None

        # check terminal state
        if legalActions:

          if util.flipCoin(self.epsilon):

            action = random.choice(legalActions)

          else:

            action=self.getPolicy(state)

            if action is None:

              action = random.choice(legalActions)

        return action

    def update(self, state, action, nextState, reward):

        # get legal actions from current state 
        legalActions = self.getLegalActions(state)


        if len(legalActions) == 0:

          self.qval[state,None] = reward

        
        if state:

          self.numChoose[state,action] += 1

          # get the maximum qvalue for the next state
          maximum_qval = self.getValue(nextState)

          # get the qvalue for this state,action pair
          curr_qval = self.getQValue(state,action)

          # print curr_qval, maximum_qval

          self.qval[state,action] = curr_qval + self.alpha*(reward+self.discount*maximum_qval-curr_qval)
          # print self.qval[state,action]
            

    def getPolicy(self, state):
        return self.computeActionFromQValues(state)

    def getValue(self, state):
        return self.computeValueFromQValues(state)

    def translate(self,action):
        direction = ""
        if action == (0,1):
          direction = "E"
        elif action == (0,-1):
          direction = "W"
        elif action == (1,0):
          direction = "S"
        elif action == (-1,0):
          direction = "N"
        return direction

    def uncertainty_action(self,action):

        cumulative_sum_weight = 0.0
        cumulative_sum_weight_arr = []

        for i in range(0, len(self.move_list)):
            cumulative_sum_weight += self.probs[i]
            cumulative_sum_weight_arr.append(cumulative_sum_weight)

        # prob = random.random()
        prob = random.uniform(0,1)

        index = bisect.bisect(cumulative_sum_weight_arr, prob)

        direct = ""
        if index == 0:
          direct = "F"  
        elif index == 1:
          direct = "B"
        elif index == 2:
          direct = "L"
        elif index == 3:
          direct = "R"

        direction = self.translate(action)
        actual_action = action
        if direction == "E":
          if direct == "F":
            actual_action = (0,1)
          elif direct == "B":
            actual_action = (0,-1)
          elif direct == "L":
            actual_action = (-1,0)
          elif direct == "R":
            actual_action = (1,0)

        elif direction == "W":
          if direct == "F":
            actual_action = (0,-1)
          elif direct == "B":
            actual_action = (0,1)
          elif direct == "L":
            actual_action = (1,0)
          elif direct == "R":
            actual_action = (-1,0)

        elif direction == "S":
          if direct == "F":
            actual_action = (1,0)
          elif direct == "B":
            actual_action = (-1,0)
          elif direct == "L":
            actual_action = (0,1)
          elif direct == "R":
            actual_action = (0,-1)

        elif direction == "N": 
          if direct == "F":
            actual_action = (-1,0)
          elif direct == "B":
            actual_action = (1,0)
          elif direct == "L":
            actual_action = (0,-1)
          elif direct == "R":
            actual_action = (0,1)

        return actual_action

    def getAvailablePos(self):
      available =[]
      for i in range(self.height):
        for j in range(self.width):
          if [i,j] not in self.walls and [i,j] not in self.pits and [i,j] != self.goal:
            available.append((i,j))
      return available

    def qlearning(self):
      available = self.getAvailablePos()
      pos_start = random.choice(available)

      self.num_steps = 0

      cur_pos = pos_start

      while self.num_steps < self.max_steps:

        pos_start = random.choice(available)
        cur_pos = pos_start
        # print cur_pos

        while True:

          if [cur_pos[0],cur_pos[1]] == self.goal:
            # pos_start = random.choice(available)
            # cur_pos = pos_start
            self.num_steps += 1
            break

          action = self.getAction(cur_pos)

          actual_action = action
          # print self.translate(action)
          

          if self.uncertainty:
            actual_action = self.uncertainty_action(action)

          next_pos = (cur_pos[0] + actual_action[0] , cur_pos[1] + actual_action[1])
          # print "next pos", next_pos

          reward = self.rewards["step"]

          flag = False
          
          if next_pos[0] < 0 or next_pos[0] >= self.height or next_pos[1] < 0 or next_pos[1] >= self.width:
            flag = True
            next_pos = cur_pos
            reward += self.rewards["wall"]
            # print "wall"
            
          elif [next_pos[0],next_pos[1]] in self.walls:
            next_pos = cur_pos
            flag = True
            reward += self.rewards["wall"]  
            # print "wall"

          elif [next_pos[0],next_pos[1]] in self.pits:
            # next_pos = cur_pos
            flag = True
            reward = self.rewards["pit"]  
            # print "pit"

          elif [next_pos[0],next_pos[1]] == self.goal:
            # next_pos = cur_pos
            reward = self.rewards["goal"]

          # print "reward: ", reward

          self.update(cur_pos, action, next_pos, reward)

          # print "updated state action qval", cur_pos, action, self.qval[cur_pos, action]

          if not flag:
            cur_pos = next_pos
        
        # self.num_steps += 1

      # print self.qval
      # return the final policy map

      # print self.qval

      self.policy_map = []
      self.policy = []

      for i in range(self.height):
        policy_list = [] 

        for j in range(self.width):      
        
          if [i,j] == self.goal:
            policy_list.append("GOAL")
            self.policy.append("GOAL")
        
          elif [i,j] in self.walls:
            policy_list.append("WALL")
            self.policy.append("WALL")

          elif [i,j] in self.pits:
            policy_list.append("PIT")
            self.policy.append("PIT")

          else:
            policy_list.append( self.translate(self.getPolicy((i,j))) )
            self.policy.append(self.translate(self.getPolicy((i,j))))
            # policy_list.append(self.getPolicy((i,j)))
        
        self.policy_map.append(policy_list)

      # print self.policy_map

      target = open("policy_list","wb")
      target.write("%s" %self.policy_map)
      target.close()

      target = open("path_list","wb")
      for path in self.policy_map:
        for p in path:
          target.write("%6s " %p)
          target.write('\n')

      target.close()

      return self.policy