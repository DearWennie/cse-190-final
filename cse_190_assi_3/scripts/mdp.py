#!/usr/bin/env python

import rospy
from read_config import read_config


class Grid():
	def __init__(self, Type, val):
		self.Type = Type
		self.curr_val = val
		self.prev_val = 0.0
		self.policy = ""


class Map():
	def __init__(self, width, height, walls, pits, goal, start, move_list, max_iter, thres, reward, prob, factor):
		self.inner_map = [[Grid("empty", 0) for x in range(width)] for y in range(height)]
		self.start = start
		self.goal = goal
		self.pits = pits
		self.walls = walls
		self.max_iter = max_iter
		self.threshold = thres
		self.reward = reward
		self.prob = prob
		self.discount_factor = factor
		self.move_list = move_list
		self.height = height
		self.width = width
		self.counter = 0

		for y in range(self.height):
			for x in range(self.width):
				if [y, x] == self.goal:
					self.inner_map[y][x].Type = "goal"
					self.inner_map[y][x].value = self.reward["goal"]
				elif [y, x] in self.pits:
					self.inner_map[y][x].Type = "pit"
					self.inner_map[y][x].value = self.reward["pit"]
				elif [y, x] in self.walls:
					self.inner_map[y][x].Type = "wall"
					self.inner_map[y][x].value = self.reward["wall"]
		# self.total_val = self.total()

	def total_diff(self):
		tot = 0.0
		for y in range(self.height):
			for x in range(self.width):
				tot += abs(self.inner_map[y][x].curr_val - self.inner_map[y][x].prev_val)
		return tot

	def policy_iter(self):
		self.policy_arr = []

		for y in range(self.height):
			for x in range(self.width):
				if self.inner_map[y][x].Type == "goal":
					self.policy_arr.append("GOAL")
				elif self.inner_map[y][x].Type == "pit":
					self.policy_arr.append("PIT")
				elif self.inner_map[y][x].Type == "wall":
					self.policy_arr.append("WALL")
				else:
					self.policy_arr.append(self.inner_map[y][x].policy)

	def value_iter(self, y, x):

		v_f = self.get_reward(y-1, x, [y, x])
		v_b = self.get_reward(y+1, x, [y, x])
		v_l  = self.get_reward(y, x-1, [y, x])
		v_r  = self.get_reward(y, x+1, [y, x])

		move_north = self.prob["f"] * v_f + self.prob["l"] * v_l + self.prob["r"] * v_r + self.prob["b"] * v_b

		move_south = self.prob["f"] * v_b + self.prob["l"] * v_r + self.prob["r"] * v_l + self.prob["b"] * v_f

		move_west = self.prob["f"] * v_l + self.prob["l"] * v_b + self.prob["r"] * v_f + self.prob["b"] * v_r

		move_east = self.prob["f"] * v_r + self.prob["l"] * v_f + self.prob["r"] * v_b + self.prob["b"] * v_l

		v_star = max(move_north, move_south, move_west, move_east)

		self.inner_map[y][x].curr_val = v_star

		if v_star == move_north:
			self.inner_map[y][x].policy = "N"
		elif v_star == move_south:
			self.inner_map[y][x].policy = "S"
		elif v_star == move_west:
			self.inner_map[y][x].policy = "W"
		elif v_star == move_east:
			self.inner_map[y][x].policy = "E"


	def get_reward(self, row, col, pos):
		if row == -1 or row == self.height or col == -1 or col == self.width:
			return self.discount_factor * self.inner_map[pos[0]][pos[1]].prev_val + self.reward["wall"]

		if self.inner_map[row][col].Type == "wall":
			return self.discount_factor * self.inner_map[pos[0]][pos[1]].prev_val + self.reward["wall"]

		if self.inner_map[row][col].Type == "pit":
			return self.reward["step"] + self.reward["pit"]

		if self.inner_map[row][col].Type == "goal":
			return self.reward["step"] + self.reward["goal"]

		return self.discount_factor * self.inner_map[row][col].prev_val + self.reward["step"]

	def mdp_process(self):

		while self.counter <= self.max_iter:

			for y in range(self.height):
				for x in range(self.width):
					self.inner_map[y][x].prev_val = self.inner_map[y][x].curr_val

					if self.inner_map[y][x].Type == "goal" or self.inner_map[y][x].Type == "pit" or self.inner_map[y][x].Type == "wall":
						continue

					self.value_iter(y, x)

			new_tot_diff = self.total_diff()
			if new_tot_diff <= self.threshold:
				break

			# self.total_val = new_tot
			self.counter += 1

		# for y in range(self.height):
		#   for x in range(self.width):
		#     self.inner_map[y][x].prev_val = self.inner_map[y][x].curr_val

		self.policy_iter()
		return self.policy_arr

class MDP():

	def __init__(self, config):
		self.config = config
		self.mdp_map = self.construct_map()
		self.mdp_policy = self.mdp_map.mdp_process()

	def construct_map(self):
		self.start = self.config["start"]
		self.goal = self.config["goal"]
		self.pits = self.config["pits"]
		self.walls = self.config["walls"]
		self.max_iter = self.config["max_iterations"]
		self.threshold = self.config["threshold_difference"]
		self.reward = {}
		self.reward["step"] = self.config["reward_for_each_step"]
		self.reward["wall"] = self.config["reward_for_hitting_wall"]
		self.reward["goal"] = self.config["reward_for_reaching_goal"]
		self.reward["pit"] = self.config["reward_for_falling_in_pit"] 
		self.prob = {}
		self.prob["f"] = self.config["prob_move_forward"]
		self.prob["b"] = self.config["prob_move_backward"]
		self.prob["l"] = self.config["prob_move_left"]
		self.prob["r"] = self.config["prob_move_right"]
		self.discount_factor = self.config["discount_factor"]
		self.move_list = self.config["move_list"]
		self.height = self.config["map_size"][0]
		self.width = self.config["map_size"][1]

		return Map(self.width, self.height, self.walls, self.pits, self.goal, self.start, self.move_list, self.max_iter, self.threshold, self.reward, self.prob, self.discount_factor)


