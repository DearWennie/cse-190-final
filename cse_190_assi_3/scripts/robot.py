#!/usr/bin/env python

import rospy
from read_config import read_config
from astar import AStar
from mdp import MDP
from qlearning import QLearningAgent
import image_util as img
from cse_190_assi_3.msg import AStarPath, PolicyList
from std_msgs.msg import Bool


class Robot():
	def __init__(self):
		rospy.init_node("robot")
		rospy.sleep(1)

		self.config = read_config()

		self.path_pub = rospy.Publisher(
		"/results/path_list",
		AStarPath,
		queue_size = 10
		)

		self.policy_pub = rospy.Publisher(
		"/results/policy_list",
		PolicyList,
		queue_size = 10
		)

		self.shutdown_pub = rospy.Publisher(
		"/map_node/sim_complete",
		Bool,
		queue_size = 10
		)

		# self.AStar = AStar(self.config)
		# if self.AStar.has_astar:
		# 	for grid in self.AStar.astar_map.astar_path:
		# 		print grid
		# 		self.AStar_msg = AStarPath()
		# 		self.AStar_msg.data = grid
		# 		rospy.sleep(1)
		# 		self.path_pub.publish(self.AStar_msg)

		# self.mdp = MDP(self.config)
		# self.mdp_policys = self.mdp.mdp_policy
		# self.policy_msg = PolicyList()
		# self.policy_msg.data = self.mdp_policys
		# rospy.sleep(5)
		# self.policy_pub.publish(self.policy_msg)

		self.qlearning = QLearningAgent(0.3, 0.35, True)
		policy = self.qlearning.qlearning()
		self.policy_msg = PolicyList()
		self.policy_msg.data = policy
		rospy.sleep(5)
		self.policy_pub.publish(self.policy_msg)

		done_signal = Bool()
		done_signal.data = True
		self.shutdown_pub.publish(done_signal)
		rospy.sleep(1)
		rospy.signal_shutdown("All done")

    


if __name__ == "__main__":
  rbt = Robot()

