ó
®·VWc           @   s/   d  d l  Z  d e f d     YZ d   Z d S(   iÿÿÿÿNt   Counterc           B   sz   e  Z d  Z d   Z d   Z d   Z d   Z d   Z d   Z d   Z	 d   Z
 d	   Z d
   Z d   Z d   Z RS(   s¼  
    A counter keeps track of counts for a set of keys.
 
    The counter class is an extension of the standard python
    dictionary type.  It is specialized to have number values
    (integers or floats), and includes a handful of additional
    functions to ease the task of counting data.  In particular,
    all keys are defaulted to have value 0.  Using a dictionary:
 
    a = {}
    print a['test']
 
    would give an error, while the Counter class analogue:
 
    >>> a = Counter()
    >>> print a['test']
    0
 
    returns the default 0 value. Note that to reference a key
    that you know is contained in the counter,
    you can still use the dictionary syntax:
 
    >>> a = Counter()
    >>> a['test'] = 2
    >>> print a['test']
    2
 
    This is very useful for counting things without initializing their counts,
    see for example:
 
    >>> a['blah'] += 1
    >>> print a['blah']
    1
 
    The counter also includes additional functionality useful in implementing
    the classifiers for this assignment.  Two counters can be added,
    subtracted or multiplied together.  See below for details.  They can
    also be normalized and their total count and arg max can be extracted.
    c         C   s    |  j  | d  t j |  |  S(   Ni    (   t
   setdefaultt   dictt   __getitem__(   t   selft   idx(    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyR   +   s    c         C   s%   x | D] } |  | c | 7<q Wd S(   sÔ   
        Increments all elements of keys by the same count.
 
        >>> a = Counter()
        >>> a.incrementAll(['one','two', 'three'], 1)
        >>> a['one']
        1
        >>> a['two']
        1
        N(    (   R   t   keyst   countt   key(    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   incrementAll/   s    c         C   sf   t  |  j    d k r d S|  j   } g  | D] } | d ^ q/ } | j t |   } | | d S(   s9   
        Returns the key with the highest value.
        i    i   N(   t   lenR   t   Nonet   itemst   indext   max(   R   t   allt   xt   valuest   maxIndex(    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   argMax=   s     c         C   s@   |  j    } d   } | j d |  g  | D] } | d ^ q, S(   s(  
        Returns a list of keys sorted by their values.  Keys
        with the highest values will appear first.
 
        >>> a = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> a['third'] = 1
        >>> a.sortedKeys()
        ['second', 'third', 'first']
        c         S   s   t  | d |  d  S(   Ni   (   t   sign(   R   t   y(    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   <lambda>T   s    t   cmpi    (   R   t   sort(   R   t   sortedItemst   compareR   (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt
   sortedKeysG   s    	c         C   s   t  |  j    S(   s9   
        Returns the sum of counts for all keys.
        (   t   sumR   (   R   (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt
   totalCountX   s    c         C   sO   t  |  j    } | d k r" d Sx& |  j   D] } |  | | |  | <q/ Wd S(   sä   
        Edits the counter such that the total count of all
        keys sums to 1.  The ratio of counts for all keys
        will remain the same. Note that normalizing an empty
        Counter will result in an error.
        i    N(   t   floatR   R   (   R   t   totalR   (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt	   normalize^   s
     c         C   s1   t  |  } x |  D] } |  | c | :<q Wd S(   s/   
        Divides all counts by divisor
        N(   R   (   R   t   divisorR   (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt	   divideAllj   s    c         C   s   t  t j |    S(   s/   
        Returns a copy of the counter
        (   R    R   t   copy(   R   (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyR#   r   s    c         C   sq   d } |  } t  |  t  |  k r4 | | } } n  x6 | D]. } | | k rS q; n  | | | | | 7} q; W| S(   s  
        Multiplying two counters gives the dot product of their vectors where
        each unique label is a vector element.
 
        >>> a = Counter()
        >>> b = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> b['first'] = 3
        >>> b['second'] = 5
        >>> a['third'] = 1.5
        >>> a['fourth'] = 2.5
        >>> a * b
        14
        i    (   R
   (   R   R   R   R   R   (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   __mul__x   s    c         C   s1   x* | j    D] \ } } |  | c | 7<q Wd S(   s`  
        Adding another counter to a counter increments the current counter
        by the values stored in the second counter.
 
        >>> a = Counter()
        >>> b = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> a += b
        >>> a['first']
        1
        N(   R   (   R   R   R   t   value(    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   __radd__   s    c         C   s   t    } xA |  D]9 } | | k r; |  | | | | | <q |  | | | <q Wx. | D]& } | |  k rl qT n  | | | | <qT W| S(   sZ  
        Adding two counters gives a counter with the union of all keys and
        counts of the second added to counts of the first.
 
        >>> a = Counter()
        >>> b = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> (a + b)['first']
        1
        (   R    (   R   R   t   addendR   (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   __add__¤   s    	c         C   s   t    } xA |  D]9 } | | k r; |  | | | | | <q |  | | | <q Wx2 | D]* } | |  k rl qT n  d | | | | <qT W| S(   sq  
        Subtracting a counter from another gives a counter with the union of all keys and
        counts of the second subtracted from counts of the first.
 
        >>> a = Counter()
        >>> b = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> (a - b)['first']
        -5
        iÿÿÿÿ(   R    (   R   R   R'   R   (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   __sub__¾   s    	(   t   __name__t
   __module__t   __doc__R   R	   R   R   R   R    R"   R#   R$   R&   R(   R)   (    (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyR       s   '			
								c         C   s   t  j    } | |  k  S(   N(   t   random(   t   pt   r(    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   flipCoinØ   s    (   R-   R   R    R0   (    (    (    s6   /home/mji/catkin_ws/src/cse_190_assi_3/scripts/util.pyt   <module>   s   Õ