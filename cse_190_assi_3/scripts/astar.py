# astar implementation needs to go here
#!/usr/bin/env python
import rospy
import math
import heapq
from read_config import read_config

class Grid():
	def __init__(self,y,x,goal):
		self.visited = False
		self.y = y
		self.x = x
		self.goal = goal
		self.h = self.calcManhantan()
		self.g = 0
		self.f = self.h + self.g
		self.wall = False
		self.pit = False
		self.prev = [] 

	def calcManhantan(self):
		x_diff = abs(self.x - self.goal[1])
		y_diff = abs(self.y - self.goal[0])
		return x_diff + y_diff

class Map():
	def __init__(self, width, height, walls, pits, goal, start, move_list):
		self.inner_map = [[Grid(y,x, goal) for x in range(width)] for y in range(height)]
		self.goal = goal
		self.walls = walls
		self.pits = pits
		self.width = width
		self.height = height
		self.start = start
		self.move_list = move_list
		self.astar_path = []
		for [y,x] in self.walls:
			self.inner_map[y][x].wall = True
		for [y,x] in self.pits:
			self.inner_map[y][x].pit = True

	def astar_search(self):

		open_list = []
		# close_list = []

		start_grid = self.inner_map[self.start[0]][self.start[1]]
		# path = []
		start_grid.g = 0
		# open_list.push(self.start, 0)
		heapq.heappush(open_list, (0, self.start, start_grid))

		while open_list:

			f, frontier, frontier_grid = heapq.heappop(open_list)
			# heappush(close_list, frontier)
			if frontier == self.goal:
				self.construct_path(self.goal, self.inner_map[self.goal[0]][self.goal[1]])
				return True
				
			curr_grid = frontier_grid
			curr_grid.visited = True
			neighbors = self.explore(frontier)

			for neighbor in neighbors:
				# print self.walls, self.pits, frontier, neighbor
				neighbor_grid = self.inner_map[neighbor[0]][neighbor[1]]
				neighbor_grid.g = curr_grid.g + 1
				neighbor_grid.prev = curr_grid
				neighbor_grid.f = curr_grid.g + neighbor_grid.h

				open_neighbor = None
				flag_find = False
				for item in open_list:
					if item[1] == neighbor:
						open_neighbor = item[2]
						flag_find = True
						break
				if not flag_find:
					heapq.heappush(open_list, (neighbor_grid.f, neighbor, neighbor_grid))
				else:
					if open_neighbor.f > neighbor_grid.f:
						open_neighbor.f = neighbor_grid.f
						open_neighbor.prev = neighbor_grid.prev
		# return False
		return False

	def construct_path(self, goal, goal_grid):
		path = [goal]
		curr = goal_grid
		while curr.prev :
			curr = curr.prev
			path.append([curr.y, curr.x])
		path.reverse()
		self.astar_path = path
		# return path.reverse()

	def explore(self, grid):
		y = grid[0]
		x = grid[1]
		frontier = []

		for move in self.move_list:
			delta_y, delta_x = move
			new_x = x + delta_x
			new_y = y + delta_y

			if new_x >= 0 and new_x < self.width and new_y >= 0 and new_y < self.height:
				if not self.inner_map[new_y][new_x].visited and not self.inner_map[new_y][new_x].wall and not self.inner_map[new_y][new_x].pit:
					frontier.append([new_y,new_x])

		return frontier

class AStar():

	def __init__(self, config):
		self.config = config
		self.astar_map = self.construct_map()
		self.has_astar = self.astar_map.astar_search()
		# self.map = construct_map()

	def construct_map(self):
		walls = self.config["walls"]
		pits = self.config["pits"]
		goal = self.config["goal"]
		height = self.config["map_size"][0]
		width = self.config["map_size"][1]
		start = self.config["start"]
		move_list = self.config['move_list']
		return Map(width, height, walls, pits, goal, start, move_list)  